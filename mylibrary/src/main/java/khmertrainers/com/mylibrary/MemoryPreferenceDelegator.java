package khmertrainers.com.mylibrary;

import java.util.HashMap;

public class MemoryPreferenceDelegator {
    private static MemoryPreferenceDelegator mInstance;
    private static HashMap<String, Object> mMemoyPrefMap;

    public static MemoryPreferenceDelegator getInstance() {
        if (mInstance == null) {
            mInstance = new MemoryPreferenceDelegator();
            mMemoyPrefMap = new HashMap<>();
        }
        return mInstance;
    }

    public boolean contains(String key) {
        return mMemoyPrefMap.containsKey(key);
    }

    public String get(String key) {
        if (mMemoyPrefMap.containsKey(key)) {
            return (String) mMemoyPrefMap.get(key);
        }
        return "";
    }

    public void put(String key, Object value) {
        mMemoyPrefMap.put(key, value);
    }

}
