package khmertrainers.com.bizplay.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import khmertrainers.com.bizplay.R;
import khmertrainers.com.mylibrary.MemoryPreferenceDelegator;

public class LoginActivity extends AppCompatActivity {
    ImageView imageView;
    EditText tv_username,tv_password;
    Button btnLogin;
    MemoryPreferenceDelegator mMemory;
    String name = "dddd";
    String pass = "4444";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*set object reference for ui*/
        imageView = findViewById(R.id.imageView);
        tv_username = findViewById(R.id.username);
        tv_password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.login);

        mMemory = MemoryPreferenceDelegator.getInstance();
        mMemory.put("key_name",name);
        mMemory.put("key_pass",pass);

        /*Set event for login*/
        btnLogin.setOnClickListener(v->{
            getDaataLogin();
        });
    }

    private void getDaataLogin() {
        String nameIntent = tv_username.getText().toString();
        String passIntent = tv_password.getText().toString();
        if (tv_username.getText().toString().equals(mMemory.get("key_name")) &&
            tv_password.getText().toString().equals(mMemory.get("key_pass"))){

            Intent intent = new Intent(LoginActivity.this,StorageMemoryActivity.class);
            intent.putExtra("name",nameIntent);
            intent.putExtra("pass",passIntent);
            startActivity(intent);

        }else {
            /*clear text from editText*/
            tv_username.setText("");
            tv_password.setText("");
            Toast.makeText(this, "Please input again!", Toast.LENGTH_SHORT).show();
            Log.d("o","Username:"+nameIntent);
            Log.d("o","password:"+passIntent);
        }
    }
}
