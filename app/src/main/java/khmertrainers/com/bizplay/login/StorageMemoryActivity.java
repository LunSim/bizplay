package khmertrainers.com.bizplay.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import khmertrainers.com.bizplay.R;

public class StorageMemoryActivity extends AppCompatActivity {
    TextView tv_usernameG,tv_passG;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage_memory);

        tv_passG = findViewById(R.id.passwordG);
        tv_usernameG = findViewById(R.id.usernameG);

        /*Create for get Data after Intent*/
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String pass = intent.getStringExtra("pass");

        /*catch data for display*/
        tv_usernameG.setText("Username :"+name);
        tv_passG.setText("Password :"+pass);
    }
}
